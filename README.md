# Slips's dwmblocks config.
As of 12/14/21.

Also on [my local server](https://git.slipfox.xyz/dwmblocks)

# INSTALLATION:
Set your location in the sb-weather module and type `make install` to install everything.

The sb-cam module detects whether or not the uvcvideo module is loaded. Use the togglecam command to toggle uvcvideo on and off.

# Dependencies:

sensors

pamixer

NetworkManager

curl 

dwm (I recommend my [dwm config!](https://git.envs.net/Slips/dwm))
