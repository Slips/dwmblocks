PREFIX ?= /usr/local
CC ?= cc
LDFLAGS = -lX11

output: dwmblocks.c blocks.def.h blocks.h
	${CC}  dwmblocks.c $(LDFLAGS) -o dwmblocks
blocks.h:
	cp blocks.def.h $@


clean:
	rm -f *.o *.gch dwmblocks
install: output
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 dwmblocks $(DESTDIR)$(PREFIX)/bin/dwmblocks
	install -m 0755 sb-volume $(DESTDIR)$(PREFIX)/bin/sb-volume
	install -m 0755 sb-temp $(DESTDIR)$(PREFIX)/bin/sb-temp
	install -m 0755 sb-net $(DESTDIR)$(PREFIX)/bin/sb-net
	install -m 0755 sb-weather $(DESTDIR)$(PREFIX)/bin/sb-weather
	install -m 0755 sb-battery $(DESTDIR)$(PREFIX)/bin/sb-battery
	install -m 0755 sb-rss $(DESTDIR)$(PREFIX)/bin/sb-rss
	install -m 0755 sb-cam $(DESTDIR)$(PREFIX)/bin/sb-cam
	install -m 0755 togglecam $(DESTDIR)$(PREFIX)/bin/togglecam
	# If you forgot to set your location in sb-weather, do that now. the path to the script is $(DESTDIR)$(PREFIX)/bin/sb-weather.
uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dwmblocks
